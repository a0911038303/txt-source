隔天，艾達一早就到訪並說，有個要花三天的工作，所以今天開始有三天不能來。

這一天，用了採取到的藥草、苔蘚、果實的調藥就全都結束了。

品嘗著午後的茶，雷肯終於，說出了那決定性的話語。

那是雷肯一直煩惱著，該在什麼時候講的話。
要是說了出來，就可能得結束與希拉的師徒關係。不只如此，還有可能會被殺掉。但是，雷肯思考許久得出的結論是，遵從內心的想法，也就是，不能把這句話憋在心裡。

雷肯還有另一個選項。那就是馬上離開城鎮，對希拉的事情貫徹沉默。
但雷肯還是，選擇說出口。

「希拉。」
「怎麼了？」
「你之前說，用不了〈淨化〉。」
「是有說過。」
「但你沒有說，沒有習得〈淨化〉。」
「然後呢？」
「之所以用不了〈淨化〉，是因為會把自身給毀滅掉嗎？」

希拉喝了口茶，並閉上雙眼。
宛如在細細品味著口中的茶，過了一陣子後，才開了口。

「什麼時候，發現的。」
「最初從這座城鎮的城門進來的時候。」
「嘿？」
「我有一種能力，能廣範圍探知人類、動物、魔獸及妖魔。」
「我有注意到喔。」
「這個探知，會將普通的人類表示為淡淡的赤色。持有魔力的人類會是強烈的赤光。動物是綠色，而魔獸和妖魔則是青色。剛進入城鎮時，有一個宛如大型迷宮的深階層的頭目的，強烈的青光。那甚至是能匹敵龍種的光。」
「⋯⋯是嗎。之前就注意到，跟我在一起的時候，你總是微妙地緊張著，原來是這麼一回事嗎。完全沒想過會存在那種探知能力。果然異世界是個可怕的地方阿。」
「我不知道你為什麼要裝作人類，為什麼要住在人類的城鎮裡。不過，聽到了吟遊詩人的歌曲。魔女艾爾希拉的故事。」
「啊啊，那個嗎。」
「原來你以前是人類阿。一直到現在，也作為人類活著。」
「就算我是怪物，也不代表那個傳說的魔女艾爾希拉跟我是同一人物吧？」
「我也不知道該怎麼講，該說是理解了嗎，在我的心中，啊啊原來如此，拼圖就拼了起來。我的直覺給我的結論是，你就是艾爾希拉。」
「直覺嗎。」
「似乎有種說法叫不死人，你正是所謂不死人的妖魔阿。但是，又跟生來就會無差別地襲擊人的妖魔不同。」
「幽鬼族的妖魔，在某些場合原本也是人類。我也跟那一樣吧。但是我呢，作為人類時的意識和記憶還強烈地殘存著。就只是這樣的差別而已。」

希拉接著，緩緩地訴說了自己的故事。
希拉說出的事實，和雷肯從吟遊詩人聽到的歌曲，故事的流向大致相同。但是，細節可說是完全不一樣。

艾爾希拉是地方貴族的女兒。因為一些瑣事被王都的貴族看上，治癒魔法的才能和美貌又得到了不錯的評價，便被決定要入住王宮。

最初的預定是讓艾爾希拉成為第一王子的側室。但是，被偉大的瑪哈札爾王看上後，艾爾希拉成了王的寵妃。
艾爾希拉對生活沒有不滿。照料瑪哈札爾王的起居是無上的名譽，王也帶來了很多優秀的魔法使和魔道書，滿足了艾爾希拉的學習慾。艾爾希拉在任何系統的魔法都發揮了才能，被師傅們喻為百年難得一見的天才。


某一天，王集合了最高峰的魔道研究者們並下了令。去尋找能讓艾爾希拉的美貌與才能長存於世的方法吧。這個不合理的難題，賢者們僅僅花了三年就得到了答案。然而，需要內藏魔力多得讓人難以置信的魔石，而且那顆魔石還必須曾伴隨在死者身邊百年以上。

那種魔石的所在，王正好知道。那就是，收藏在初代王的墳墓裡的古代龍的魔石。
艾爾希拉反對了。因為她理解得懂，賢者們想出的計畫，並非讓生者成為永恆的生者，而是奪走生者的生命，化其為不死者，奪去生命的變化。
然而，在王的命令下，秘術還是實行了。艾爾希拉成了不死者，其美貌與能力成了永遠之物。王非常滿意。

如果只是這樣的話，說不定就沒什麼問題。艾爾希拉打算，等王死後，自己要躺在王的棺材旁邊。同時，接受神聖的光之洗禮。

數年後，王有了這樣的想法。自己也想成為永恆。

王要求賢者們，往古代龍的魔石再度注入魔力，對自己行使同樣的秘術。賢者們反對了。秘術雖然能夠再現，但上次之所以能成功，是因為被施予者的艾爾希拉有著大量魔力又年輕，而對王的施術恐怕不會成功。不只如此，艾爾希拉在施術之前，有對自己使用〈淨化〉。身體因此被引導至最佳狀態。然而，艾爾希拉成了不死者，已經無法使用〈淨化〉。艾爾希拉的替補，只能使用階位低很多的〈淨化〉。

儘管如此，在王的命令下，秘術還是施展了。王雖然得到了永恆的生命，但失去了記憶與知識。成了宛如壊掉人偶的存在。
艾爾希拉告訴了宰相事實，尋求協助。

───

宰相馬上開始了將王位讓給第一王子的手續。同時，命令艾爾希拉對王施展魔法〈支配〉。〈支配〉是精神系魔法的最上位，給予簡單的指令，再以其為基礎來進行複雜的動作，施術者就算不在受術者附近也不會有問題。雖然對偉大的王下那種魔法實在有所顧忌，但只要一下子就好，便被說服了。

但是在那之前，第一王子就死了。雖說是王子，年齡也已經大到連曾孫都有了，死亡並無可疑之處。

瓦普多王国的王位，只傳長子。瑪哈札爾王還健康時，當然希望第一王子繼位，也希望其長男薩利瑪王子能繼位。宰相想出的說詞是，讓位給第一王子是瑪哈札爾王的敕令，死者第一王子在形式上已經繼承了王位，接著便開始了讓薩利瑪王子成為新王的手續。想不到這時，薩利瑪王子做出了預料外的行動。

宣言要服喪三年，足不出戶了。

在瓦普多王国，當雙親過世時，負責繼承的長男會服喪。但通常只會形式上去進行，就算是身分顯貴之人，也頂多在爭鬥及懲處家臣上有所節制一段期間而已。但是，薩利瑪王子遵循了古老儀式，嚴謹地服喪。而且三年代表的是，當王去世時，其長男必須服喪的長度。既然瑪哈札爾王還在世，這便有所不敬，但並沒有被追究。

以宰相為首的有能的實務者們，以王的親政這樣的方式來施政。艾爾希拉，只得維持〈支配〉整整三年。

服喪終於期滿後，薩利瑪王子乞求謁見瑪哈札爾王。艾爾希拉沒有被允許同席。薩利瑪王子憎恨著本來要做父親第一王子側室，卻輕易地攏絡了瑪哈札爾王，成為寵妃的艾爾希拉。

謁見的會場發生了悲劇。

───

察覺異變，趕到會場的艾爾希拉看到的是，沉在血海中的宰相、護衛、以及薩利瑪王子的部下，還有正要手刃瑪哈札爾王的薩利瑪王子。
艾爾希拉在一瞬間對薩利瑪王子施展〈硬直〉，救了王。但是宰相已經完全沒救了。

詢問了薩利瑪王子後，真相終於大白了。薩利瑪王子憎恨著瑪哈札爾王。因為年老的瑪哈札爾王一直都把持著王位，原本應當成王的父親，最終沒能得到王位。薩利瑪王子為了替敬愛的父親復仇，決意暗殺王。

艾爾希拉不知道該怎麼辦。能給出適切指示的宰相已經不在。但不能不讓王給出敕命。

艾爾希拉決定好了要給的敕命。
敕命是，不追究薩利瑪王子的兇行，準備妥當後，就把王位讓給薩利瑪王子。
之後才發現，艾爾希拉這時應該給予薩利瑪王子嚴厲的處置。這樣的話，仰慕宰相的人們，估計就能原諒薩利瑪王子。

然而，薩利瑪王子沒有付出任何代價就被放行了。因此，宰相指揮下的所有人，都憎恨著薩利瑪王子。就連宰相的政敵們，以及跟宰相衝突不斷的將軍們，都對薩利瑪王子感到厭惡。

在那之後，一切都變得混亂不堪。

已經沒有人記得當時的詳情了。
注意到的時候，薩利瑪王子已經率領著軍隊攻擊了王都。

此時，六位王孫中有四人守護著王宮。站在薩利瑪王子一方的，只有親弟弟。

薩利瑪王子的勢力不大，能被很簡單地收拾掉。但是，現實並非如此。因為艾爾希拉是薩利瑪王子的同伴。王發出的敕命全都對薩利瑪王子一夥有利，內戰被不斷拉長。

之後才想到，應該要將薩利瑪王子幽閉，立其他的王孫為新王。但是當時的艾爾希拉沒能有那種決斷，只是愚昧地遵從瑪哈札爾王過去的意志。

国中四處都發生了紛爭。
表面上看似和平的王国，其背後早已種滿了爭鬥的火種了吧。
最後，薩利瑪王子終於壓制了王宮。

艾爾希拉悄悄逃走了。原因是，要從遠方觀察事態，讓王在適當的場面死去，最後再讓自己死去。如果自己先死的話，人們會看到化為悲慘人偶的王的姿態。只有這件事絶對不能發生。

瑪哈札爾王，被薩利瑪王子燒死了。

然而在那之後，薩利瑪王子又再次做出了預料外的行動。
他自殺了。
由於過度的震驚與失望，艾爾希拉甚至忘了自我了斷，只是在人煙稀少的山中到處流浪著。

然後，漫長的歳月過去了。